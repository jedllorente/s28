/* retrieve all todo list items */
fetch('https://jsonplaceholder.typicode.com/todos', {})
    .then((response) => response.json())
    .then((json) => console.log(json))


/* create an array using map method to return title of every item */
fetch('https://jsonplaceholder.typicode.com/todos', {})
    .then((response) => response.json())
    .then((json) => console.log(json.map((title) => {
        return title.title;
    })))
    /* retrieve single todo list item */
fetch('https://jsonplaceholder.typicode.com/todos/1')
    .then((response) => response.json())
    .then((json) => console.log(json))

/* print message in console that displays title and status */
fetch('https://jsonplaceholder.typicode.com/todos', {})
    .then((response) => response.json())
    .then((json) => json.map((title, status) => {
        console.log(title.title);
    }))

/* POST method create a to do list item */
fetch('https://jsonplaceholder.typicode.com/todos', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
            title: 'New Post',
            body: 'Christmas is Coming.',
            userId: 1
        })
    })
    .then((response) => response.json())
    .then((json) => console.log(json))

/* PUT update a to do list item using */

fetch('https://jsonplaceholder.typicode.com/todos/1', {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
            title: 'Updated Post',
            description: 'Christmas is Going.',
            status: true,
            dateCompleted: '10/26/2021',
            userId: 1
        })
    })
    .then((response) => response.json())
    .then((json) => console.log(json))

/*u update by changing data structure  && add when dateCompleted*/
fetch('https://jsonplaceholder.typicode.com/todos/1', {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
            title: 'Updated Post',
            body: 'Hello World Once More',
            userId: 1,
            dateCompleted: '10/26/2021'
        })
    })
    .then((response) => response.json())
    .then((json) => console.log(json))

/* PATCH update a to do list */
fetch('https://jsonplaceholder.typicode.com/todos/1', {
        method: 'PATCH',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
            title: 'Updated Post',
            body: 'Hello World Once More',
            userId: 1,
            dateCompleted: '10/26/2021'
        })
    })
    .then((response) => response.json())
    .then((json) => console.log(json))